﻿using Nancy.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DemonWsMutualSer
{
    class Program
    {
        static async Task Main(string[] args)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();

            string root = Directory.GetCurrentDirectory();
            // If directory does not exist, don't even try   
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }

            Log.Logger = new LoggerConfiguration()
                .WriteTo.File($"{root}\\Log.txt")
                .MinimumLevel.Verbose()
                .CreateLogger();
            Log.Debug("Inicio DemonWs....");

            int x = 1;

            DateTime fecha_hoy = DateTime.Now.Date;
            DateTime difFechas = fecha_hoy.AddDays(-3);
            int y = 0;
            try
            {
                do
                {

                    List<int> listNum = await ListNumeroCitasMutualser();

                    foreach (var item in listNum)
                    {
                        Log.Information("Numero cita por enviar: " + item);

                    }
                    //await ApiCitasMutualser(listNum);


                    Console.WriteLine(fecha_hoy + "----------" + difFechas);

                    if (true)
                    {
                        y += 1;
                        
                        if (y > 5)
                        {
                            x = 0;
                        }
                    }
                    Thread.Sleep(TimeSpan.FromSeconds(10));
                } while (x == 1);

            }
            catch (Exception e)
            {

                Log.Error(e, "Catcht Error. mian");

            }
            //for (int i = 0; i < 5; i++)
            //{
            //    saludar();
            //    Thread.Sleep(1000);
            //}

            //for (int i = 0; i < 5; i++)
            //{
            //    TaskStatus tareastatus = saludar2();
            //    Thread.Sleep(1000);
            //}



        }

        static async Task<List<int>> ListNumeroCitasMutualser() 
        {

            JavaScriptSerializer jss = new JavaScriptSerializer();
            List<int> list =   new List<int>();

            //jsonGet can2 = new jsonGet()
            //{
            //    id = 161705,
            //    historia = 0,
            //    codigo = ""
            //};


            string URL = "http://localhost:58412/WsMsF2/ApiDemonListWsMutual";
            //string URL = "http://localhost:58412/WsMsF2/TestRetry";
            //string URL = "http://192.95.39.99:8047/WsMsF2/TestRetry";
            //string URL = "http://192.95.39.99:8047/WsMsF2/ApiMSF2CreationOnlinePolly";
            //string URL = "http://localhost:58412/WsMsF2/ApiMSF2CreationOnlinePolly";


            HttpClient client = new HttpClient();

            client.Timeout = TimeSpan.FromMinutes(30);
            client.BaseAddress = new Uri(URL);
            HttpResponseMessage response = await client.GetAsync(URL);
            if (response.IsSuccessStatusCode)
            {
                var dataObjects = response.Content.ReadAsStringAsync().Result;
                dynamic dv = null;
                dv = jss.DeserializeObject(dataObjects);
            
                   // Log.Information("Se obtuvo lista correctamente. ");
                if (dv != null)
                {
                    Log.Information("Se obtuvo lista correctamente. ");
                    foreach (var item in dv)
                    {
                        list.Add(Convert.ToInt32(item));
                    }
                }
                else
                {
                    Log.Information("Se obtuvo lista vacia. ");
                }
               
            }
            else
            {
                Log.Warning("Error en api obtener lista, status " + response.IsSuccessStatusCode);

            }


            return   list;
        }
        static async Task ApiCitasMutualser(List<int> lista)
        {

            JavaScriptSerializer jss = new JavaScriptSerializer();
            List<int> list = new List<int>();

            //jsonGet can2 = new jsonGet()
            //{
            //    id = 161705,
            //    historia = 0,
            //    codigo = ""
            //};


            //string URL = "http://localhost:58412/WsMsF2/ApiDemonListWsMutual";
            //string URL = "http://localhost:58412/WsMsF2/TestRetry";
            //string URL = "http://192.95.39.99:8047/WsMsF2/TestRetry";
            //string URL = "http://192.95.39.99:8047/WsMsF2/ApiMSF2CreationOnlinePolly";
            string URL = "http://localhost:58412/WsMsF2/ApiMSF2CreationOnlinePolly";
            //CONSUMO

            try
            {
                foreach (var item in lista)
                {
                    jsonGet jg = new jsonGet()
                    {
                        id = (item),
                    };
                    string js = jss.Serialize(jg);

                    HttpClient client = new HttpClient();

                    client.Timeout = TimeSpan.FromMinutes(30);
                    client.BaseAddress = new Uri(URL);
                    var httpContent = new StringContent(js, Encoding.UTF8, "application/json");

                    HttpResponseMessage response = await client.PostAsync(URL, httpContent);
                    if (response.IsSuccessStatusCode)
                    {
                        Log.Information("Numero cita enviado: " + item);
                        var dataObjects = response.Content.ReadAsStringAsync().Result;
                        dynamic dv = null;
                        dv = jss.DeserializeObject(dataObjects);
                    }
                    else
                    {
                        Log.Warning("Error en api envio status " + response.IsSuccessStatusCode + " Numero cita : " + item);
                    }
                }
            }
            catch (Exception e)
            {

                Log.Error(e, "Catcht Error. ApiCitasMutualser"  );
            }

        }
    }
    public class jsonGet 
    {
        public int id { get; set; }
        public decimal historia { get; set; }
        public string codigo { get; set; }
    };
}
